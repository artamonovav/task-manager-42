package ru.t1.artamonov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.artamonov.tm.api.service.IConnectionService;
import ru.t1.artamonov.tm.api.service.IProjectService;
import ru.t1.artamonov.tm.api.service.ITaskService;
import ru.t1.artamonov.tm.api.service.IUserService;
import ru.t1.artamonov.tm.dto.model.UserDTO;
import ru.t1.artamonov.tm.enumerated.Role;
import ru.t1.artamonov.tm.marker.UnitCategory;
import ru.t1.artamonov.tm.util.HashUtil;

import java.util.UUID;

import static ru.t1.artamonov.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class UserServiceTest {

    @NotNull
    private final PropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final IProjectService projectService = new ProjectService(connectionService);

    @NotNull
    private final ITaskService taskService = new TaskService(connectionService);

    @NotNull
    private final IUserService userService = new UserService(connectionService, propertyService);

    @Test
    public void findOneById() {
        @NotNull UserDTO user = userService.create(UUID.randomUUID().toString(), UUID.randomUUID().toString());
        try {
            Assert.assertEquals(user.getId(), userService.findOneById(user.getId()).getId());
            Assert.assertNotEquals(ADMIN.getId(), userService.findOneById(user.getId()).getId());
        } catch (@NotNull final Exception e) {
            userService.removeById(user.getId());
        }
    }

    @Test
    public void remove() {
        @NotNull UserDTO user = userService.create(UUID.randomUUID().toString(), UUID.randomUUID().toString());
        try {
            Assert.assertTrue(userService.existsById(user.getId()));
            userService.remove(user);
            Assert.assertFalse(userService.existsById(user.getId()));
        } catch (@NotNull final Exception e) {
            userService.removeById(user.getId());
        }
    }

    @Test
    public void removeById() {
        @NotNull UserDTO user = userService.create(UUID.randomUUID().toString(), UUID.randomUUID().toString());
        try {
            Assert.assertTrue(userService.existsById(user.getId()));
            userService.removeById(user.getId());
            Assert.assertFalse(userService.existsById(user.getId()));
        } catch (@NotNull final Exception e) {
            userService.removeById(user.getId());
        }
    }

    @Test
    public void createLoginPassword() {
        @NotNull UserDTO user = userService.create(UUID.randomUUID().toString(), UUID.randomUUID().toString());
        try {
            @NotNull UserDTO createdUser = userService.findOneById(user.getId());
            Assert.assertNotNull(createdUser);
            Assert.assertEquals(createdUser.getLogin(), user.getLogin());
            Assert.assertEquals(createdUser.getPasswordHash(), user.getPasswordHash());
            Assert.assertNull(user.getEmail());
        } catch (@NotNull final Exception e) {
            userService.removeById(user.getId());
        }
    }

    @Test
    public void createLoginPasswordEmail() {
        @NotNull UserDTO user = userService.create(UUID.randomUUID().toString(), UUID.randomUUID().toString(), UUID.randomUUID().toString());
        try {
            @NotNull UserDTO createdUser = userService.findOneById(user.getId());
            Assert.assertNotNull(createdUser);
            Assert.assertEquals(createdUser.getLogin(), user.getLogin());
            Assert.assertEquals(createdUser.getPasswordHash(), user.getPasswordHash());
            Assert.assertEquals(createdUser.getEmail(), user.getEmail());
        } catch (@NotNull final Exception e) {
            userService.removeById(user.getId());
        }
    }

    @Test
    public void createLoginPasswordRole() {
        @NotNull UserDTO user = userService.create(UUID.randomUUID().toString(), UUID.randomUUID().toString(), Role.USUAL);
        try {
            @NotNull UserDTO createdUser = userService.findOneById(user.getId());
            Assert.assertNotNull(createdUser);
            Assert.assertEquals(createdUser.getLogin(), user.getLogin());
            Assert.assertEquals(createdUser.getPasswordHash(), user.getPasswordHash());
            Assert.assertEquals(createdUser.getRole(), user.getRole());
            Assert.assertNull(user.getEmail());
        } catch (@NotNull final Exception e) {
            userService.removeById(user.getId());
        }
    }

    @Test
    public void findByLogin() {
        @NotNull UserDTO user = userService.create(UUID.randomUUID().toString(), UUID.randomUUID().toString());
        Assert.assertEquals(user.getId(), userService.findByLogin(user.getLogin()).getId());
        userService.removeById(user.getId());
    }

    @Test
    public void findByEmail() {
        @NotNull UserDTO user = userService.create(UUID.randomUUID().toString(), UUID.randomUUID().toString(), UUID.randomUUID().toString());
        Assert.assertEquals(user.getId(), userService.findByEmail(user.getEmail()).getId());
        userService.removeById(user.getId());
    }

    @Test
    public void isLoginExist() {
        @NotNull UserDTO user = userService.create(UUID.randomUUID().toString(), UUID.randomUUID().toString());
        try {
            Assert.assertTrue(userService.isLoginExist(user.getLogin()));
            Assert.assertFalse(userService.isLoginExist(UNIT_TEST_INCORRECT_LOGIN));
        } catch (@NotNull final Exception e) {
            userService.removeById(user.getId());
        }
    }

    @Test
    public void isEmailExist() {
        @NotNull UserDTO user = userService.create(UUID.randomUUID().toString(), UUID.randomUUID().toString(), UUID.randomUUID().toString());
        try {
            Assert.assertTrue(userService.isEmailExist(user.getEmail()));
            Assert.assertFalse(userService.isEmailExist(UUID.randomUUID().toString()));
        } catch (@NotNull final Exception e) {
            userService.removeById(user.getId());
        }
    }

    @Test
    public void removeByLogin() {
        @NotNull UserDTO user = userService.create(UUID.randomUUID().toString(), UUID.randomUUID().toString());
        try {
            userService.removeByLogin(user.getLogin());
            Assert.assertNull(userService.findOneById(user.getId()));
        } catch (@NotNull final Exception e) {
            userService.removeById(user.getId());
        }
    }

    @Test
    public void removeByEmail() {
        @NotNull UserDTO user = userService.create(UUID.randomUUID().toString(), UUID.randomUUID().toString(), UUID.randomUUID().toString());
        try {
            userService.removeByEmail(USER1.getEmail());
            Assert.assertThrows(Exception.class, () -> userService.findOneById(USER1.getId()));
        } catch (@NotNull final Exception e) {
            userService.removeById(user.getId());
        }
    }

    @Test
    public void setPassword() {
        @NotNull UserDTO user = userService.create(UUID.randomUUID().toString(), UUID.randomUUID().toString());
        try {
            userService.setPassword(user.getId(), UNIT_TEST_USER_PASSWORD);
            @NotNull UserDTO updatedUser = userService.findOneById(user.getId());
            Assert.assertEquals(HashUtil.salt(propertyService, UNIT_TEST_USER_PASSWORD), updatedUser.getPasswordHash());
        } catch (@NotNull final Exception e) {
            userService.removeById(user.getId());
        }
    }

    @Test
    public void updateUser() {
        @NotNull UserDTO user = userService.create(UUID.randomUUID().toString(), UUID.randomUUID().toString());
        try {
            userService.updateUser(user.getId(), "Firstname", "Lastname", "Middlename");
            @NotNull UserDTO updatedUser = userService.findOneById(user.getId());
            Assert.assertEquals("Firstname", updatedUser.getFirstName());
            Assert.assertEquals("Lastname", updatedUser.getLastName());
            Assert.assertEquals("Middlename", updatedUser.getMiddleName());
        } catch (@NotNull final Exception e) {
            userService.removeById(user.getId());
        }
    }

    @Test
    public void lockUserByLogin() {
        @NotNull UserDTO user = userService.create(UUID.randomUUID().toString(), UUID.randomUUID().toString());
        try {
            Assert.assertFalse(userService.findOneById(user.getId()).getLocked());
            userService.lockUserByLogin(user.getLogin());
            Assert.assertTrue(userService.findOneById(user.getId()).getLocked());
        } catch (@NotNull final Exception e) {
            userService.removeById(user.getId());
        }
    }

    @Test
    public void unlockUserByLogin() {
        @NotNull UserDTO user = userService.create(UUID.randomUUID().toString(), UUID.randomUUID().toString());
        try {
            Assert.assertFalse(userService.findOneById(user.getId()).getLocked());
            userService.lockUserByLogin(user.getLogin());
            Assert.assertTrue(userService.findOneById(user.getId()).getLocked());
            userService.unlockUserByLogin(user.getLogin());
            Assert.assertFalse(userService.findOneById(user.getId()).getLocked());
        } catch (@NotNull final Exception e) {
            userService.removeById(user.getId());
        }
    }

}
