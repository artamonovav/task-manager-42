package ru.t1.artamonov.tm.constant;

import org.jetbrains.annotations.NotNull;
import ru.t1.artamonov.tm.dto.model.TaskDTO;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static ru.t1.artamonov.tm.constant.ProjectTestData.USER1_PROJECT1;
import static ru.t1.artamonov.tm.constant.ProjectTestData.USER2_PROJECT1;

public final class TaskTestData {

    @NotNull
    public final static TaskDTO USER1_TASK1 = new TaskDTO();

    @NotNull
    public final static TaskDTO USER1_TASK2 = new TaskDTO();

    @NotNull
    public final static TaskDTO USER1_TASK3 = new TaskDTO();

    @NotNull
    public final static TaskDTO USER2_TASK1 = new TaskDTO();

    @NotNull
    public final static TaskDTO USER2_TASK2 = new TaskDTO();

    @NotNull
    public final static TaskDTO USER2_TASK3 = new TaskDTO();

    @NotNull
    public final static TaskDTO ADMIN1_TASK1 = new TaskDTO();

    @NotNull
    public final static TaskDTO ADMIN1_TASK2 = new TaskDTO();

    @NotNull
    public final static TaskDTO ADMIN1_TASK3 = new TaskDTO();

    @NotNull
    public final static List<TaskDTO> USER1_TASK_LIST = Arrays.asList(USER1_TASK1, USER1_TASK2, USER1_TASK3);

    @NotNull
    public final static List<TaskDTO> USER2_TASK_LIST = Arrays.asList(USER2_TASK1, USER2_TASK2, USER2_TASK3);

    @NotNull
    public final static List<TaskDTO> ADMIN1_TASK_LIST = Arrays.asList(ADMIN1_TASK1, ADMIN1_TASK2, ADMIN1_TASK3);

    @NotNull
    public final static List<TaskDTO> TASK_LIST = new ArrayList<>();

    static {
        USER1_TASK_LIST.forEach(task -> task.setUserId(UserTestData.USER1.getId()));
        USER2_TASK_LIST.forEach(task -> task.setUserId(UserTestData.USER2.getId()));
        ADMIN1_TASK_LIST.forEach(task -> task.setUserId(UserTestData.ADMIN.getId()));

        USER1_TASK_LIST.forEach(task -> task.setProjectId(USER1_PROJECT1.getId()));
        USER2_TASK_LIST.forEach(task -> task.setProjectId(USER2_PROJECT1.getId()));

        TASK_LIST.addAll(USER1_TASK_LIST);
        TASK_LIST.addAll(USER2_TASK_LIST);
        TASK_LIST.addAll(ADMIN1_TASK_LIST);

        TASK_LIST.forEach(task -> task.setName("task-" + TASK_LIST.indexOf(task)));
        TASK_LIST.forEach(task -> task.setDescription("description of task-" + TASK_LIST.indexOf(task)));
    }

}
