package ru.t1.artamonov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.artamonov.tm.api.service.IConnectionService;
import ru.t1.artamonov.tm.api.service.ISessionService;
import ru.t1.artamonov.tm.dto.model.SessionDTO;
import ru.t1.artamonov.tm.marker.UnitCategory;

import static ru.t1.artamonov.tm.constant.SessionTestData.*;
import static ru.t1.artamonov.tm.constant.UserTestData.USER1;
import static ru.t1.artamonov.tm.constant.UserTestData.USER2;

@Category(UnitCategory.class)
public final class SessionServiceTest {

    @NotNull
    private final PropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final ISessionService sessionService = new SessionService(connectionService);

    @Test
    public void add() {
        sessionService.add(USER1_SESSION1);
        Assert.assertEquals(USER1_SESSION1.getId(), sessionService.findOneById(USER1_SESSION1.getId()).getId());
        sessionService.clear(USER1.getId());
    }

    @Test
    public void addList() {
        Assert.assertNotNull(sessionService.add(ADMIN1_SESSION_LIST));
        for (final SessionDTO session : ADMIN1_SESSION_LIST)
            Assert.assertEquals(session.getId(), sessionService.findOneById(session.getId()).getId());
        for (final SessionDTO session : ADMIN1_SESSION_LIST)
            sessionService.removeById(session.getId());
    }

    @Test
    public void addByUserId() {
        sessionService.add(USER1.getId(), USER1_SESSION1);
        SessionDTO session = sessionService.findOneById(USER1_SESSION1.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(USER1_SESSION1.getId(), session.getId());
        Assert.assertEquals(USER1.getId(), session.getUserId());
        sessionService.removeById(USER1_SESSION1.getId());
    }

    @Test
    public void clearByUserId() {
        sessionService.add(USER1_SESSION_LIST);
        for (final SessionDTO session : USER1_SESSION_LIST)
            Assert.assertEquals(session.getId(), sessionService.findOneById(session.getId()).getId());
        sessionService.clear(USER2.getId());
        Assert.assertFalse(sessionService.findAll().isEmpty());
        sessionService.clear(USER1.getId());
        sessionService.add(USER2_SESSION1);
        sessionService.clear(USER1.getId());
        Assert.assertFalse(sessionService.findAll().isEmpty());
        sessionService.clear(USER1.getId());
        sessionService.clear(USER2.getId());
    }

    @Test
    public void findAllByUserId() {
        sessionService.add(SESSION_LIST);
        for (final SessionDTO session : SESSION_LIST)
            Assert.assertEquals(session.getId(), sessionService.findOneById(session.getId()).getId());
        for (final SessionDTO session : SESSION_LIST)
            sessionService.removeById(session.getId());
    }

    @Test
    public void findOneByIdByUserId() {
        sessionService.add(USER1_SESSION1);
        sessionService.add(USER2_SESSION1);
        Assert.assertEquals(USER1_SESSION1.getId(), sessionService.findOneById(USER1.getId(), USER1_SESSION1.getId()).getId());
        Assert.assertThrows(Exception.class, () -> sessionService.findOneById(USER1.getId(), USER2_SESSION1.getId()).getId());
        sessionService.clear(USER1.getId());
        sessionService.clear(USER2.getId());
    }

    @Test
    public void removeByIdByUserId() {
        sessionService.add(USER1_SESSION1);
        sessionService.add(USER2_SESSION1);
        Assert.assertEquals(USER1_SESSION1.getId(), sessionService.removeById(USER1.getId(), USER1_SESSION1.getId()).getId());
        Assert.assertNotNull(sessionService.findOneById(USER2_SESSION1.getId()));
        Assert.assertNull(sessionService.findOneById(USER1_SESSION1.getId()));
        sessionService.clear(USER1.getId());
        sessionService.clear(USER2.getId());
    }

    @Test
    public void existsByIdByUserId() {
        sessionService.add(USER1_SESSION1);
        Assert.assertTrue(sessionService.existsById(USER1_SESSION1.getId()));
        Assert.assertFalse(sessionService.existsById(USER2_SESSION1.getId()));
        sessionService.clear(USER1.getId());
        sessionService.clear(USER2.getId());
    }

}
