package ru.t1.artamonov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.artamonov.tm.api.service.IConnectionService;
import ru.t1.artamonov.tm.api.service.IProjectService;
import ru.t1.artamonov.tm.api.service.IProjectTaskService;
import ru.t1.artamonov.tm.api.service.ITaskService;
import ru.t1.artamonov.tm.dto.model.TaskDTO;
import ru.t1.artamonov.tm.marker.UnitCategory;

import static ru.t1.artamonov.tm.constant.ProjectTestData.*;
import static ru.t1.artamonov.tm.constant.TaskTestData.*;
import static ru.t1.artamonov.tm.constant.UserTestData.USER1;

@Category(UnitCategory.class)
public final class ProjectTaskServiceTest {

    @NotNull
    private final PropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(connectionService);

    @NotNull
    private final IProjectService projectService = new ProjectService(connectionService);

    @NotNull
    private final ITaskService taskService = new TaskService(connectionService);

    @Before
    public void before() {
        projectService.add(USER1_PROJECT1);
        projectService.add(USER1_PROJECT2);
        taskService.add(USER1_TASK1);
        taskService.add(USER1_TASK2);
    }

    @After
    public void after() {
        taskService.removeAll(TASK_LIST);
        projectService.removeAll(PROJECT_LIST);
    }

    @Test
    public void bindTaskToProject() {
        projectTaskService.bindTaskToProject(USER1.getId(), USER1_PROJECT1.getId(), USER1_TASK1.getId());
        @NotNull TaskDTO task1 = taskService.findOneById(USER1_TASK1.getId());
        Assert.assertEquals(USER1_PROJECT1.getId(), task1.getProjectId());
        projectTaskService.bindTaskToProject(USER1.getId(), USER1_PROJECT2.getId(), USER1_TASK2.getId());
        @NotNull TaskDTO task2 = taskService.findOneById(USER1_TASK2.getId());
        Assert.assertEquals(USER1_PROJECT2.getId(), task2.getProjectId());
    }

    @Test
    public void unbindTaskFromProject() {
        bindTaskToProject();
        @NotNull TaskDTO task1 = taskService.findOneById(USER1_TASK1.getId());
        Assert.assertNotNull(task1.getProjectId());
        projectTaskService.unbindTaskFromProject(USER1.getId(), USER1_PROJECT1.getId(), task1.getId());
        @NotNull TaskDTO task2 = taskService.findOneById(USER1_TASK1.getId());
        Assert.assertNull(task2.getProjectId());
    }

    @Test
    public void removeProjectById() {
        bindTaskToProject();
        projectTaskService.removeProjectById(USER1.getId(), USER1_PROJECT1.getId());
        Assert.assertFalse(projectService.existsById(USER1_PROJECT1.getId()));
        Assert.assertFalse(taskService.existsById(USER1_TASK1.getId()));
    }

}
