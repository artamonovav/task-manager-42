package ru.t1.artamonov.tm.api.service;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManagerFactory;

public interface IConnectionService {

    @NotNull
    SqlSession getSqlConnection();

    @NotNull
    SqlSessionFactory getSqlSessionFactory();

    @NotNull
    EntityManagerFactory factory();

}
