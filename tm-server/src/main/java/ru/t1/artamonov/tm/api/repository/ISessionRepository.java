package ru.t1.artamonov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.artamonov.tm.dto.model.SessionDTO;

import java.util.List;

public interface ISessionRepository {

    @Insert("INSERT INTO tm_session (row_id, created, user_id, role) " +
            "VALUES (#{id}, #{date}, #{userId}, #{role});")
    void add(@NotNull final SessionDTO session);

    @Update("UPDATE tm_session SET row_id = #{id}, created = #{date}, user_id = #{userId}, role = #{role};")
    void update(@NotNull final SessionDTO session);

    @Delete("DELETE FROM tm_session WHERE row_id = #{id};")
    void remove(@NotNull final SessionDTO session);

    @Delete("DELETE FROM tm_session WHERE user_id = #{userId};")
    void removeByUserId(@Param("userId") @NotNull final String userId);

    @Nullable
    @Select("SELECT * FROM tm_session;")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "date", column = "created")
    })
    List<SessionDTO> findAll();

    @Nullable
    @Select("SELECT * FROM tm_session WHERE row_id = #{id} LIMIT 1;")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "date", column = "created")
    })
    SessionDTO findOneById(@Param("id") @NotNull final String id);

    @Nullable
    @Select("SELECT * FROM tm_session WHERE row_id = #{id} and user_id = #{userId} LIMIT 1;")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "date", column = "created")
    })
    SessionDTO findOneByIdUserId(@Param("userId") @NotNull final String userId, @Param("id") @NotNull final String id);

}
