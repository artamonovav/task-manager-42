package ru.t1.artamonov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.artamonov.tm.dto.model.TaskDTO;

import java.util.List;

public interface ITaskRepository {

    @Insert("INSERT INTO tm_task (row_id, created, name, descrptn, status, user_id, project_id) " +
            "VALUES (#{id}, #{created}, #{name}, #{description}, #{status}, #{userId}, #{projectId});")
    void add(@NotNull TaskDTO model);

    @Delete("TRUNCATE TABLE tm_task;")
    void clearAll();

    @Delete("DELETE FROM tm_task WHERE user_id = #{userId};")
    void clear(@Param("userId") @Nullable String userId);

    @NotNull
    @Select("SELECT COUNT(1) = 1 FROM tm_task WHERE row_id = #{id};")
    Boolean existsById(@Param("id") @NotNull String id);

    @NotNull
    @Select("SELECT COUNT(1) = 1 FROM tm_task WHERE row_id = #{id} AND user_id = #{userId};")
    Boolean existsByIdUserId(@Param("userId") @Nullable String userId, @Param("id") @Nullable String id);

    @Nullable
    @Select("SELECT * FROM tm_task;")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "description", column = "descrptn")
    })
    List<TaskDTO> findAll();

    @Nullable
    @Select("SELECT * FROM tm_task ORDER BY name;")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "description", column = "descrptn"),
            @Result(property = "projectId", column = "project_id")
    })
    List<TaskDTO> findAllOrderByName();

    @Nullable
    @Select("SELECT * FROM tm_task ORDER BY created;")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "description", column = "descrptn"),
            @Result(property = "projectId", column = "project_id")
    })
    List<TaskDTO> findAllOrderByCreated();

    @Nullable
    @Select("SELECT * FROM tm_task ORDER BY status;")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "description", column = "descrptn"),
            @Result(property = "projectId", column = "project_id")
    })
    List<TaskDTO> findAllOrderByStatus();

    @Nullable
    @Select("SELECT * FROM tm_task WHERE user_id = #{userId};")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "description", column = "descrptn"),
            @Result(property = "projectId", column = "project_id")
    })
    List<TaskDTO> findAllUserId(@Param("userId") @Nullable String userId);

    @Nullable
    @Select("SELECT * FROM tm_task WHERE user_id = #{userId} ORDER BY name;")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "description", column = "descrptn"),
            @Result(property = "projectId", column = "project_id")
    })
    List<TaskDTO> findAllOrderByNameUserId(@Param("userId") @Nullable String userId);

    @Nullable
    @Select("SELECT * FROM tm_task WHERE user_id = #{userId} ORDER BY created;")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "description", column = "descrptn"),
            @Result(property = "projectId", column = "project_id")
    })
    List<TaskDTO> findAllOrderByCreatedUserId(@Param("userId") @Nullable String userId);

    @Nullable
    @Select("SELECT * FROM tm_task WHERE user_id = #{userId} ORDER BY status;")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "description", column = "descrptn"),
            @Result(property = "projectId", column = "project_id")
    })
    List<TaskDTO> findAllOrderByStatusUserId(@Param("userId") @Nullable String userId);

    @Nullable
    @Select("SELECT * from tm_task WHERE row_id = #{id} LIMIT 1;")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "description", column = "descrptn"),
            @Result(property = "projectId", column = "project_id")
    })
    TaskDTO findOneById(@Param("id") @NotNull String id);

    @Nullable
    @Select("SELECT * from tm_task WHERE row_id = #{id} AND user_id = #{userId} LIMIT 1;")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "description", column = "descrptn"),
            @Result(property = "projectId", column = "project_id")
    })
    TaskDTO findOneByIdUserId(@Param("userId") @Nullable String userId, @Param("id") @Nullable String id);

    @Nullable
    @Select("SELECT * from tm_task WHERE user_id = #{userId} AND project_id = #{projectId};")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "description", column = "descrptn"),
            @Result(property = "projectId", column = "project_id")
    })
    List<TaskDTO> findAllByProjectId(@Param("userId") @Nullable String userId, @Param("projectId") @Nullable String projectId);

    @Select("SELECT COUNT(1) FROM tm_task;")
    long getSize();

    @Select("SELECT COUNT(1) FROM tm_task WHERE user_id = #{userId};")
    long getSizeUserId(@Param("userId") @Nullable String userId);

    @Delete("DELETE FROM tm_task WHERE row_id = #{id};")
    void remove(@NotNull TaskDTO model);

    @Delete("DELETE FROM tm_task WHERE row_id = #{id} and user_id = #{userId};")
    void removeUserId(@Param("userId") @Nullable String userId, @Nullable TaskDTO model);

    @Delete("DELETE FROM tm_task WHERE row_id = #{id};")
    void removeById(@Param("id") @NotNull String id);

    @Delete("DELETE FROM tm_task WHERE row_id = #{id} and user_id = #{userId};")
    void removeByIdUserId(@Param("userId") @Nullable String userId, @Param("id") @Nullable String id);

    @Delete("DELETE FROM tm_task WHERE project_id = #{projectId};")
    void removeTasksByProjectId(@Param("projectId") @NotNull String projectId);

    @Update("UPDATE tm_task SET created = #{created}, name = #{name}, descrptn = #{description}, status = #{status}, user_id = #{userId}, project_id = #{projectId} WHERE row_id = #{id};")
    void update(@NotNull TaskDTO model);

}
