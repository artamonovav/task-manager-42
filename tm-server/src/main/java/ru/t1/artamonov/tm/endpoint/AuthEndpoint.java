package ru.t1.artamonov.tm.endpoint;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.artamonov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.artamonov.tm.api.service.IAuthService;
import ru.t1.artamonov.tm.api.service.IServiceLocator;
import ru.t1.artamonov.tm.api.service.IUserService;
import ru.t1.artamonov.tm.dto.model.SessionDTO;
import ru.t1.artamonov.tm.dto.model.UserDTO;
import ru.t1.artamonov.tm.dto.request.UserLoginRequest;
import ru.t1.artamonov.tm.dto.request.UserLogoutRequest;
import ru.t1.artamonov.tm.dto.request.UserProfileRequest;
import ru.t1.artamonov.tm.dto.response.UserLoginResponse;
import ru.t1.artamonov.tm.dto.response.UserLogoutResponse;
import ru.t1.artamonov.tm.dto.response.UserProfileResponse;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@NoArgsConstructor
@WebService(endpointInterface = "ru.t1.artamonov.tm.api.endpoint.IAuthEndpoint")
public class AuthEndpoint extends AbstractEndpoint implements IAuthEndpoint {

    public AuthEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private IAuthService getAuthService() {
        return getServiceLocator().getAuthService();
    }

    @NotNull
    private IUserService getUserService() {
        return getServiceLocator().getUserService();
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public UserLoginResponse login(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserLoginRequest request
    ) {
        @Nullable final String token = getAuthService().login(request.getLogin(), request.getPassword());
        return new UserLoginResponse(token);
    }

    @NotNull
    @Override
    @WebMethod
    public UserLogoutResponse logout(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserLogoutRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        getAuthService().logout(session);
        return new UserLogoutResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public UserProfileResponse profile(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserProfileRequest request
    ) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final UserDTO user = getUserService().findOneById(userId);
        return new UserProfileResponse(user);
    }

}
