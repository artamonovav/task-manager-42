package ru.t1.artamonov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.artamonov.tm.enumerated.Role;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_user")
public final class User extends AbstractModel {

    @Nullable
    @Column(length = 50)
    private String login;

    @Nullable
    @Column(name = "pswrd_hash", length = 200)
    private String passwordHash;

    @Nullable
    @Column(length = 200)
    private String email;

    @Nullable
    @Column(name = "fst_name", length = 100)
    private String firstName;

    @Nullable
    @Column(name = "lst_name", length = 100)
    private String lastName;

    @Nullable
    @Column(name = "mdl_name", length = 100)
    private String middleName;

    @NotNull
    @Column
    @Enumerated(EnumType.STRING)
    private Role role = Role.USUAL;

    @NotNull
    @Column(name = "lock_flg")
    private Boolean locked = false;

    @NotNull
    @OneToMany(mappedBy = "user")
    private List<Task> tasks = new ArrayList<>();

    @NotNull
    @OneToMany(mappedBy = "user")
    private List<Project> projects = new ArrayList<>();

    @NotNull
    @OneToMany(mappedBy = "user")
    private List<Session> sessions = new ArrayList<>();

}
