package ru.t1.artamonov.tm.dto.response;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public final class DataXmlSaveJaxBResponse extends AbstractResponse {
}
