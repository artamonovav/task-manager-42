package ru.t1.artamonov.tm.dto.request;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public final class DataBase64LoadRequest extends AbstractUserRequest {

    public DataBase64LoadRequest(@Nullable String token) {
        super(token);
    }

}
