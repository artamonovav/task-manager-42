package ru.t1.artamonov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.artamonov.tm.api.model.IWBS;
import ru.t1.artamonov.tm.enumerated.Status;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_task")
public final class Task extends AbstractUserOwnedModel implements IWBS {

    @NotNull
    @Column(length = 100)
    private String name = "";

    @NotNull
    @Column(name = "descrptn", length = 2000)
    private String description = "";

    @NotNull
    @Column
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @Nullable
    @Column(name = "project_id", length = 50)
    private String projectId;

    @NotNull
    @Column
    private Date created = new Date();

    @NotNull
    @ManyToOne
    @JoinColumn(name = "project_id")
    private Project project;

}
