package ru.t1.artamonov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.artamonov.tm.dto.request.UserRegistryRequest;
import ru.t1.artamonov.tm.enumerated.Role;
import ru.t1.artamonov.tm.dto.model.UserDTO;
import ru.t1.artamonov.tm.util.TerminalUtil;

public final class UserRegistryCommand extends AbstractUserCommand {

    @NotNull
    private static final String NAME = "user-registry";

    @NotNull
    private static final String DESCRIPTION = "registry user";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[USER REGISTRY]");
        System.out.print("ENTER LOGIN: ");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.print("ENTER PASSWORD: ");
        @NotNull final String password = TerminalUtil.nextLine();
        System.out.print("ENTER EMAIL: ");
        @NotNull final String email = TerminalUtil.nextLine();
        @NotNull final UserRegistryRequest request = new UserRegistryRequest(getToken());
        request.setLogin(login);
        request.setPassword(password);
        request.setEmail(email);
        @NotNull final UserDTO user = getUserEndpointClient().registryUser(request).getUser();
        showUser(user);
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
