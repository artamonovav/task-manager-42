package ru.t1.artamonov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.artamonov.tm.dto.request.UserChangePasswordRequest;
import ru.t1.artamonov.tm.enumerated.Role;
import ru.t1.artamonov.tm.util.TerminalUtil;

public final class UserChangePasswordCommand extends AbstractUserCommand {

    @NotNull
    private static final String NAME = "change-password";

    @NotNull
    private static final String DESCRIPTION = "change current user password";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[CHANGE PASSWORD]");
        System.out.print("NEW PASSWORD: ");
        @NotNull final String newPassword = TerminalUtil.nextLine();
        @NotNull final UserChangePasswordRequest request = new UserChangePasswordRequest(getToken());
        request.setPassword(newPassword);
        getUserEndpointClient().changeUserPassword(request);
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
