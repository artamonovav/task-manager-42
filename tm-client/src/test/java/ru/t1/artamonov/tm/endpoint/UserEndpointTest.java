package ru.t1.artamonov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.artamonov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.artamonov.tm.api.endpoint.IUserEndpoint;
import ru.t1.artamonov.tm.dto.request.*;
import ru.t1.artamonov.tm.dto.response.*;
import ru.t1.artamonov.tm.marker.IntegrationCategory;
import ru.t1.artamonov.tm.dto.model.UserDTO;

@Category(IntegrationCategory.class)
public final class UserEndpointTest {

    @NotNull
    private final IUserEndpoint userEndpoint = IUserEndpoint.newInstance();

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance();

    @Nullable
    private String adminToken;

    @Nullable
    private String userToken;

    @Before
    public void init() {
        @NotNull final UserLoginResponse adminResponse = authEndpoint.login(
                new UserLoginRequest("admin", "admin")
        );
        adminToken = adminResponse.getToken();
        @NotNull final UserLoginResponse userResponse = authEndpoint.login(
                new UserLoginRequest("test", "test")
        );
        userToken = userResponse.getToken();
    }

    @Test
    public void changeUserPassword() {
        final String newPassword = "test2";
        Assert.assertThrows(Exception.class, () -> userEndpoint.changeUserPassword(
                new UserChangePasswordRequest())
        );
        Assert.assertThrows(Exception.class, () -> userEndpoint.changeUserPassword(
                new UserChangePasswordRequest(null))
        );
        Assert.assertThrows(Exception.class, () -> userEndpoint.changeUserPassword(
                new UserChangePasswordRequest("wrongUserToken"))
        );
        @Nullable final UserChangePasswordRequest request = new UserChangePasswordRequest(userToken);
        request.setPassword(newPassword);
        @Nullable final UserChangePasswordResponse response = userEndpoint.changeUserPassword(request);
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getUser());
        @Nullable final UserLoginResponse loginCheck = authEndpoint.login(
                new UserLoginRequest("test", newPassword)
        );
        Assert.assertTrue(loginCheck.getSuccess());
        request.setPassword("test");
        @Nullable final UserChangePasswordResponse undoResponse = userEndpoint.changeUserPassword(request);
        Assert.assertNotNull(undoResponse);
        Assert.assertNotNull(undoResponse.getUser());
    }

    @Test
    public void lockUser() {
        Assert.assertThrows(Exception.class, () -> userEndpoint.lockUser(
                new UserLockRequest())
        );
        Assert.assertThrows(Exception.class, () -> userEndpoint.lockUser(
                new UserLockRequest(null))
        );
        Assert.assertThrows(Exception.class, () -> userEndpoint.lockUser(
                new UserLockRequest(""))
        );
        Assert.assertThrows(Exception.class, () -> userEndpoint.lockUser(
                new UserLockRequest(null, null))
        );
        Assert.assertThrows(Exception.class, () -> userEndpoint.lockUser(
                new UserLockRequest("", ""))
        );
        Assert.assertThrows(Exception.class, () -> userEndpoint.lockUser(
                new UserLockRequest("wrongToken", ""))
        );
        Assert.assertThrows(Exception.class, () -> userEndpoint.lockUser(
                new UserLockRequest(userToken, "wrongLogin"))
        );
        Assert.assertThrows(Exception.class, () -> userEndpoint.lockUser(
                new UserLockRequest(userToken, "test"))
        );
        @Nullable final UserLockResponse response = userEndpoint.lockUser(
                new UserLockRequest(adminToken, "test")
        );
        Assert.assertNotNull(response);
        @Nullable final UserProfileResponse userProfileResponse = authEndpoint.profile(
                new UserProfileRequest(userToken)
        );
        Assert.assertNotNull(userProfileResponse);
        Assert.assertNotNull(userProfileResponse.getUser());
        Assert.assertTrue(userProfileResponse.getUser().getLocked());
        userEndpoint.unlockUser(new UserUnlockRequest(adminToken, "test"));
    }

    @Test
    public void unlockUser() {
        Assert.assertThrows(Exception.class, () -> userEndpoint.unlockUser(
                new UserUnlockRequest())
        );
        Assert.assertThrows(Exception.class, () -> userEndpoint.unlockUser(
                new UserUnlockRequest(null))
        );
        Assert.assertThrows(Exception.class, () -> userEndpoint.unlockUser(
                new UserUnlockRequest(""))
        );
        Assert.assertThrows(Exception.class, () -> userEndpoint.unlockUser(
                new UserUnlockRequest(null, null))
        );
        Assert.assertThrows(Exception.class, () -> userEndpoint.unlockUser(
                new UserUnlockRequest("", ""))
        );
        Assert.assertThrows(Exception.class, () -> userEndpoint.unlockUser(
                new UserUnlockRequest("wrongToken", ""))
        );
        Assert.assertThrows(Exception.class, () -> userEndpoint.unlockUser(
                new UserUnlockRequest(userToken, "wrongLogin"))
        );
        Assert.assertThrows(Exception.class, () -> userEndpoint.unlockUser(
                new UserUnlockRequest(userToken, "test"))
        );
        @Nullable final UserLockResponse lockResponse = userEndpoint.lockUser(
                new UserLockRequest(adminToken, "test")
        );
        Assert.assertNotNull(lockResponse);
        @Nullable final UserUnlockResponse unlockResponse = userEndpoint.unlockUser(
                new UserUnlockRequest(adminToken, "test")
        );
        Assert.assertNotNull(unlockResponse);
        @Nullable final UserProfileResponse userProfileResponse = authEndpoint.profile(
                new UserProfileRequest(userToken)
        );
        Assert.assertNotNull(userProfileResponse);
        Assert.assertNotNull(userProfileResponse.getUser());
        Assert.assertFalse(userProfileResponse.getUser().getLocked());
    }

    @Test
    public void registryUser() {
        Assert.assertThrows(Exception.class, () -> userEndpoint.registryUser(
                new UserRegistryRequest()
        ));
        Assert.assertThrows(Exception.class, () -> userEndpoint.registryUser(
                new UserRegistryRequest(null, "login", "pass", "email")
        ));
        Assert.assertThrows(Exception.class, () -> userEndpoint.registryUser(
                new UserRegistryRequest("", "login", "pass", "email")
        ));
        Assert.assertThrows(Exception.class, () -> userEndpoint.registryUser(
                new UserRegistryRequest("qwe", "login", "pass", "email")
        ));
        Assert.assertThrows(Exception.class, () -> userEndpoint.registryUser(
                new UserRegistryRequest(userToken, null, "pass", "email")
        ));
        Assert.assertThrows(Exception.class, () -> userEndpoint.registryUser(
                new UserRegistryRequest(userToken, "login", null, "email")
        ));
        Assert.assertThrows(Exception.class, () -> userEndpoint.registryUser(
                new UserRegistryRequest(userToken, "login", "pass", "email")
        ));
        @Nullable final UserRegistryResponse response = userEndpoint.registryUser(
                new UserRegistryRequest(userToken, "temp", "pass", "emails")
        );
        Assert.assertNotNull(response.getUser());
        @Nullable final UserDTO user = response.getUser();
        Assert.assertNotNull(user.getLogin());
        Assert.assertEquals("temp", user.getLogin());
        @Nullable final UserLoginResponse login = authEndpoint.login(
                new UserLoginRequest("temp", "pass")
        );
        Assert.assertTrue(login.getSuccess());
    }

    @Test
    public void removeUser() {
        Assert.assertThrows(Exception.class, () -> userEndpoint.removeUser(
                new UserRemoveRequest())
        );
        Assert.assertThrows(Exception.class, () -> userEndpoint.removeUser(
                new UserRemoveRequest(null, null))
        );
        Assert.assertThrows(Exception.class, () -> userEndpoint.removeUser(
                new UserRemoveRequest("", null))
        );
        Assert.assertThrows(Exception.class, () -> userEndpoint.removeUser(
                new UserRemoveRequest("wrongToken", null))
        );
        Assert.assertThrows(Exception.class, () -> userEndpoint.removeUser(
                new UserRemoveRequest(userToken, null))
        );
        Assert.assertThrows(Exception.class, () -> userEndpoint.removeUser(
                new UserRemoveRequest(adminToken, null))
        );
        Assert.assertThrows(Exception.class, () -> userEndpoint.removeUser(
                new UserRemoveRequest(adminToken, ""))
        );
        @Nullable final UserLoginResponse loginResponse = authEndpoint.login(
                new UserLoginRequest("temp", "pass")
        );
        Assert.assertTrue(loginResponse.getSuccess());
        @Nullable final UserRemoveResponse response = userEndpoint.removeUser(
                new UserRemoveRequest(adminToken, "temp")
        );
        Assert.assertNotNull(response);
        Assert.assertThrows(Exception.class, () -> authEndpoint.login(
                new UserLoginRequest("temp", "pass"))
        );
    }

    @Test
    public void updateUserProfile() {
        Assert.assertThrows(Exception.class, () -> userEndpoint.updateUserProfile(
                new UserUpdateProfileRequest()
        ));
        Assert.assertThrows(Exception.class, () -> userEndpoint.updateUserProfile(
                new UserUpdateProfileRequest(null, "first", "last", "middle")
        ));
        Assert.assertThrows(Exception.class, () -> userEndpoint.updateUserProfile(
                new UserUpdateProfileRequest("", "first", "last", "middle")
        ));
        Assert.assertThrows(Exception.class, () -> userEndpoint.updateUserProfile(
                new UserUpdateProfileRequest("234", "first", "last", "middle")
        ));
        @Nullable final UserUpdateProfileResponse response = userEndpoint.updateUserProfile(
                new UserUpdateProfileRequest(userToken, "update", "profile", "user")
        );
        Assert.assertNotNull(response.getUser());
        @Nullable final UserDTO user = response.getUser();
        Assert.assertNotNull(user.getLogin());
        Assert.assertEquals("profile", user.getLastName());
        userEndpoint.updateUserProfile(new UserUpdateProfileRequest(
                userToken, "user", "return", "value"
        ));
    }

}
